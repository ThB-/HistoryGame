﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatBox : MonoBehaviour
{
    public int maxMessages = 25;
    public InputField chat;

    public GameObject chatPanel, textObject;
    [SerializeField]
    List<Message> liste_de_message = new List<Message>();
    void Start()
    {
        
    }

    void Update()
    {
        if (chat.text !="")
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                SendMessageToChat(chat.text);
                chat.text = "";
            }
        }
        else
        {
            if(!chat.isFocused && Input.GetKeyDown(KeyCode.Return))
            {
                chat.ActivateInputField();
            } 
        }
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            SendMessageToChat("J'appuie sur entrer!");
        }
    }

    public void SendMessageToChat(string texte)
    {
        if (liste_de_message.Count >= maxMessages)
        {
            Destroy(liste_de_message[0].textObject.gameObject);
        }
        Message newMessage = new Message();

        newMessage.mess = texte;

        GameObject newText = Instantiate(textObject, chatPanel.transform);

        newMessage.textObject = newText.GetComponent<Text>();
        newMessage.textObject.text = newMessage.mess;

        liste_de_message.Add(newMessage);
    }
}
